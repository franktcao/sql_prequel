from google.cloud import bigquery

# Create a "Client" object
client = bigquery.Client()

# Construct a reference to the "hacker_news" dataset
dataset_ref = client.dataset("hacker_news", project="bigquery-public-data")

## API request - fetch the dataset ( you can use client.list_tables(dataset) )
#dataset = client.get_dataset(dataset_ref)

# Construct a reference to the "comments" table
table_ref = dataset_ref.table("comments")

# API request - fetch the table
table = client.get_table(table_ref)

## Preview the first five lines of the "comments" table
#df = client.list_rows(table, max_results=5).to_dataframe()
#print(df)
#
#schema = table.schema
#print(schema)
#print(df.columns)

GB = 10**9

## Query to select comments that received more than 10 replies
#query_popular = """
#                SELECT parent, COUNT(id)
#                FROM `bigquery-public-data.hacker_news.comments`
#                GROUP BY parent
#                HAVING COUNT(id) > 10
#                """
## Set up the query (cancel the query if it would use too much of 
## your quota, with the limit set to 10 GB)
#safe_config = bigquery.QueryJobConfig(maximum_bytes_billed=10*GB)
#query_job   = client.query(query_popular, job_config=safe_config)
#
## API request - run the query, and convert the results to a pandas DataFrame
#popular_comments = query_job.to_dataframe()
#
## Print the first five rows of the DataFrame
#head = popular_comments.head()
#print(head)


# Improved version of earlier query, now with aliasing & improved readability
query_improved = """
                 SELECT parent, COUNT(1) AS NumPosts
                 FROM `bigquery-public-data.hacker_news.comments`
                 GROUP BY parent
                 HAVING COUNT(1) > 10
                 """

safe_config = bigquery.QueryJobConfig(maximum_bytes_billed=10*GB)
query_job   = client.query(query_improved, job_config=safe_config)

# API request - run the query, and convert the results to a pandas DataFrame
improved_df = query_job.to_dataframe()

# Print the first five rows of the DataFrame
head = improved_df.head()
print(head)



