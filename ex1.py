from google.cloud import bigquery
import numpy as np

# Create a "Client" object
client = bigquery.Client()

# Construct a reference to the "hacker_news" dataset
dataset_ref = client.dataset("chicago_crime", project="bigquery-public-data")

# API request - fetch the dataset
dataset = client.get_dataset(dataset_ref)

# List all the tables in the "hacker_news" dataset
tables = list(client.list_tables(dataset))
ntables = len(tables)

# Print names of all tables in the dataset (there are four!)
for table in tables:
  print(table.table_id)

# Construct a reference to the "crime" table
table_ref = dataset_ref.table("crime")

# API request - fetch the table
table = client.get_table(table_ref)

## Print information on all the columbs in the "full" table in the "hacker_news" dataset
schema = table.schema
for schem in schema:
  #if schem[type] == 'TIMESTAMP':
    print('\t', end="")
    print(schem)


# Preview the first 10 lines and 2 columns of the "crime" table
mebana = client.list_rows(table, selected_fields=table.schema[:2], max_results=10).to_dataframe()
print(mebana)

# Preview the first 100 lines of the "full" table
df = client.list_rows(table, max_results=100).to_dataframe()
print(df.columns)
#df.to_json("himan.json")
#df.to_csv("himan.csv")
#print(df)
#print(df['date'])
#
##df_temp = df["TIMESTAMP" >= 0 ]
#df_temp = df[np.isfinite(df['date'])]

#
## Preview the first five entries in the "by" column of the "full" table
#mebana = client.list_rows(table, selected_fields=table.schema[:2], max_results=10).to_dataframe()
#print(mebana)
#
#
