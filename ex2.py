from google.cloud import bigquery
import numpy as np
import pandas as pd

####################################################################################################
# Setting up
# Create a "Client" object
client = bigquery.Client()

# Construct a reference to the "hacker_news" dataset
dataset_ref = client.dataset("openaq", project="bigquery-public-data")

# API request - fetch the dataset
dataset = client.get_dataset(dataset_ref)

# List all the tables in the "hacker_news" dataset
tables = list(client.list_tables(dataset))
ntables = len(tables)

# Print names of all tables in the dataset (there are four!)
for table in tables:
  print(table.table_id)

table_ref = dataset_ref.table('global_air_quality')
table = client.get_table(table_ref)
#print(client.list_rows(table).to_dataframe())

####################################################################################################


# Query examples
##################################################

# Select city column from table 'global_air_quality' from dataset 'bigquery-public-data' where the country is 'US'
query = """
        SELECT city
        FROM `bigquery-public-data.openaq.global_air_quality`
        WHERE country = 'US'
        """
# Select city and country columns from table 'global_air_quality' from dataset 'bigquery-public-data' where the country is 'US'
query = """
        SELECT city, country
        FROM `bigquery-public-data.openaq.global_air_quality`
        WHERE country = 'US'
        """
# Select all columns from table 'global_air_quality' from dataset 'bigquery-public-data' where the country is 'US'
query = """
        SELECT * 
        FROM `bigquery-public-data.openaq.global_air_quality`
        WHERE country = 'US'
        """

# Select distinct elements from the 'country' column from table 'global_air_quality' from dataset 'bigquery-public-data' where the 'unit' is 'ppm'
first_query = 
        """
        SELECT DISTINCT country
        FROM `bigquery-public-data.openaq.global_air_quality`
        WHERE unit = 'ppm'
        """

first_results = open_aq.query_to_pandas_safe(first_query)

# View top few rows of results
print(first_results.head())

