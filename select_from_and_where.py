from google.cloud import bigquery
import numpy as np
import pandas as pd

# Create a "Client" object
client = bigquery.Client()

# Construct a reference to the "hacker_news" dataset
dataset_ref = client.dataset("openaq", project="bigquery-public-data")

# API request - fetch the dataset
dataset = client.get_dataset(dataset_ref)

# List all the tables in the "hacker_news" dataset
tables = list(client.list_tables(dataset))
ntables = len(tables)

# Print names of all tables in the dataset (there are four!)
for table in tables:
  print(table.table_id)

table_ref = dataset_ref.table('global_air_quality')
table = client.get_table(table_ref)
#print(client.list_rows(table).to_dataframe())


# Query examples
##################################################

# Select city column from table 'global_air_quality' from dataset 'bigquery-public-data' where the country is 'US'
query = """
        SELECT city
        FROM `bigquery-public-data.openaq.global_air_quality`
        WHERE country = 'US'
        """
# Select city and country columns from table 'global_air_quality' from dataset 'bigquery-public-data' where the country is 'US'
query = """
        SELECT city, country
        FROM `bigquery-public-data.openaq.global_air_quality`
        WHERE country = 'US'
        """
# Select all columns from table 'global_air_quality' from dataset 'bigquery-public-data' where the country is 'US'
query = """
        SELECT * 
        FROM `bigquery-public-data.openaq.global_air_quality`
        WHERE country = 'US'
        """
print(query)

# Set up the query 
query_job = client.query(query)

# API request - run the query, and return a pandas DataFrame
us_cities = query_job.to_dataframe()

# What five cities have the most measurements?
print(us_cities.city.value_counts().head())
#print((us_cities.city.sort_values()))
pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_rows', 100)
#print(us_cities[us_cities['city'] == 'Boston'])
#print(us_cities.groupby('city'))
#us_cities['city'].astype('category')
#print(us_cities.sort_values('city'))
sorted_cities = us_cities.sort_values('city')
print(sorted_cities['city'].unique())
print(us_cities[us_cities['city'] == 'Boston-Cambridge-Quincy'])
boston_stats = us_cities[us_cities['city'] == 'Boston-Cambridge-Quincy']
print(boston_stats)


boston_stats.to_csv("himan.csv")
boston_stats.to_json("himan.json")

# Query to get the score column from every row where the type column has value "job"
query = """
        SELECT score, title
        FROM `bigquery-public-data.hacker_news.full`
        WHERE type = "job" 
        """

# Create a QueryJobConfig object to estimate size of query without running it
dry_run_config = bigquery.QueryJobConfig(dry_run=True)

# API request - dry run query to estimate costs
dry_run_query_job = client.query(query, job_config=dry_run_config)

print("This query will process {} MB.".format(dry_run_query_job.total_bytes_processed/10**6))


# Only run the query if it's less than 100 MB
ONE_HUNDRED_MB = 100*1000*1000
#safe_config = bigquery.QueryJobConfig(maximum_bytes_billed=ONE_HUNDRED_MB)
#
## Set up the query (will only run if it's less than 100 MB)
#safe_query_job = client.query(query, job_config=safe_config)
#
## API request - try to run the query, and return a pandas DataFrame
#safe_query_job.to_dataframe()

# Only run the query if it's less than 1 GB
ONE_GB = 1000*1000*1000
safe_config = bigquery.QueryJobConfig(maximum_bytes_billed=ONE_GB)

# Set up the query (will only run if it's less than 1 GB)
safe_query_job = client.query(query, job_config=safe_config)

# API request - try to run the query, and return a pandas DataFrame
job_post_scores = safe_query_job.to_dataframe()

# Print average score for job posts
print(job_post_scores.score.mean())
